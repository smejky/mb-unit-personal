function reportLoaded()
{
    fixAttachmentLinksOnIE();
}

function fixAttachmentLinksOnIE()
{
    if (needFixupForIE())
    {
        // On IE, pages in the local filesystem that possess the Mark of the Web
        // are forbidden from navigating to other local files.  This breaks links
        // to attachments on the local filesystem unless we make some changes.
        var count = document.links.length;
        for (var i = 0; i < count; i++)
        {
            var link = document.links[i];
            var href = link.href;
            if (link.className == "attachmentLink" && isLocalFileUri(href))
            {
                link.href = toGallioAttachmentUri(href);
            }
        }
    }
}

function toGallioAttachmentUri(uri)
{
    var path = uri.substring(8).replace(/\//g, "\\");
    return "gallio:openAttachment?path=" + path;
}

var needFixupForIECache = undefined;
function needFixupForIE()
{
    if (needFixupForIECache == undefined)
        needFixupForIECache = isIE() && (isLocalFileUri(window.location.href) || isInMemoryUri(window.location.href));
        
    return needFixupForIECache;
}

function isIE()
{
    return navigator.appName == "Microsoft Internet Explorer";
}

function isLocalFileUri(uri)
{
    return uri.search(/^file:\/\/\//) == 0;
}

function isInMemoryUri(uri)
{
    return uri == "about:blank";
}

function toggle(id)
{
    var icon = document.getElementById('toggle-' + id);
    if (icon != null)
    {
        var childElement = document.getElementById(id);
        if (icon.src.indexOf('Plus.gif') != -1)
        {
            icon.src = icon.src.replace('Plus.gif', 'Minus.gif');
            if (childElement != null)
                childElement.style.display = "block";
        }
        else
        {
            icon.src = icon.src.replace('Minus.gif', 'Plus.gif');
            if (childElement != null)
                childElement.style.display = "none";
        }
    }
    /*
    */
}

function expand(ids)
{
    for (var i = 0; i < ids.length; i++)
    {
        var id = ids[i];
        var icon = document.getElementById('toggle-' + id);
        if (icon != null)
        {
            if (icon.src.indexOf('Plus.gif') != -1)
            {
                icon.src = icon.src.replace('Plus.gif', 'Minus.gif');

                var childElement = document.getElementById(id);
                if (childElement != null)
                    childElement.style.display = "block";
            }
        }
    }
    
}

function navigateTo(path, line, column)
{
    var navigator = new ActiveXObject("Gallio.Navigator.GallioNavigator");
    if (navigator)
        navigator.NavigateTo(path, line, column);
}

function setInnerHTMLFromUri(id, uri)
{
    _asyncLoadContentFromUri(uri, function(loadedDocument)
    {
        // workaround for IE failure to auto-detect HTML content
        var children = isIE() ? loadedDocument.body.children : null;
        if (children && children.length == 1 && children[0].tagName == "PRE")
        {
            var text = getTextContent(loadedDocument.body);
            setInnerHTMLFromContent(id, text);
        }
        else
        {
            var html = loadedDocument.body.innerHTML;
            setInnerHTMLFromContent(id, html);
        }
    });
}

function setPreformattedTextFromUri(id, uri)
{
    _asyncLoadContentFromUri(uri, function(loadedDocument) {setPreformattedTextFromContent(id, getTextContent(loadedDocument.body));});
}

function setInnerHTMLFromHiddenData(id)
{
    var element = document.getElementById(id + '-hidden');
    if (element)
        setInnerHTMLFromContent(id, getTextContent(element));
}

function setPreformattedTextFromHiddenData(id)
{
    var element = document.getElementById(id + '-hidden');
    if (element)
        setPreformattedTextFromContent(id, getTextContent(element));
}

function setInnerHTMLFromContent(id, content)
{
    if (content != undefined)
    {
        var element = document.getElementById(id);
        if (element)
            element.innerHTML = content;
    }
}

function setPreformattedTextFromContent(id, content)
{
    if (content != undefined)
    {
        var element = document.getElementById(id);
        if (element)
        {
            element.innerHTML = "<pre></pre>";
            setTextContent(element.children[0], content);
        }
    }
}

function getTextContent(element)
{
    return element.textContent != undefined ? element.textContent : element.innerText;
}

function setTextContent(element, content)
{
    if (element.textContent != undefined)
        element.textContent = content;
    else
        element.innerText = content;
}

function setFrameLocation(frame, uri)
{
    if (frame.contentWindow)
        frame.contentWindow.location.replace(uri);
}

function _asyncLoadContentFromUri(uri, callback)
{
    var asyncLoadFrame = document.getElementById('_asyncLoadFrame');

    if (!asyncLoadFrame.pendingRequests)
        asyncLoadFrame.pendingRequests = [];

    asyncLoadFrame.pendingRequests.push({uri: uri, callback: callback});

    _asyncLoadFrameNext(asyncLoadFrame);
}

function _asyncLoadFrameOnLoad()
{
    var asyncLoadFrame = document.getElementById('_asyncLoadFrame');
    if (asyncLoadFrame)
    {
        var request = asyncLoadFrame.currentRequest;
        if (request)
        {
            asyncLoadFrame.currentRequest = undefined;

            try
            {
                var loadedWindow = asyncLoadFrame.contentWindow;
                if (loadedWindow && loadedWindow.location.href != "about:blank")
                {
                    var loadedDocument = loadedWindow.document;
                    if (loadedDocument)
                    {
                        request.callback(loadedDocument);
                    }
                }
            }
            catch (ex)
            {
                //alert(ex.message);
            }
        }

        _asyncLoadFrameNext(asyncLoadFrame);
    }
}

function _asyncLoadFrameNext(asyncLoadFrame)
{
    while (!asyncLoadFrame.currentRequest && asyncLoadFrame.pendingRequests && asyncLoadFrame.pendingRequests.length > 0)
    {
        var request = asyncLoadFrame.pendingRequests.shift();
        asyncLoadFrame.currentRequest = request;

        try
        {
            setFrameLocation(asyncLoadFrame, request.uri);
        }
        catch (ex)
        {
            //alert(ex.message);
        }
    }
}


/****************ACCESS PANEL CONTROLL**********************/

//default search input content
var DEFAULT_SEARCH_TEXT = "";

//default param for ordering tests
var DEFAULT_ORDER_TYPE = "alphabet";

//sets onload function
window.onload = prepareAccessPanel;

//list of roots of test trees, see TestPart comment
var lists = Array ();

//actual order param
var orderType = DEFAULT_ORDER_TYPE;


//values from filters
var showPassed = true;
var showFailed = true;
var showIgnored = true;

//this function is called after page loads
function prepareAccessPanel (){ 
    
    addAccessPanel();
    
    //sets default search input value
    var searchInput = document.getElementsByClassName("searchText")[0];
    searchInput.value = DEFAULT_SEARCH_TEXT;
    
    //finds first testRunContainer and parse all tests info from summary into forest
    var list = document.getElementsByClassName("testStepRunContainer")[0];
    lists = parseTests(list);
    
    //fill the forest with actual data for the first time
    recountTests(lists[0]);
   
}

//counts actual numbers of failed, passed and condensed tests for all nodes (children) of list
function recountTests(list) {
    
    //list has no children, it is test. Find result in the inner content.
    if (list.children.length == 0) {
        if (list.node.getElementsByClassName("condensed")[0].title == "failed") list.sortParams["testsFailed"]++;
        if (list.node.getElementsByClassName("condensed")[0].title == "passed") list.sortParams["testsPassed"]++;
        if (list.node.getElementsByClassName("condensed")[0].title == "ignored") list.sortParams["testsIgnored"]++;
    } else {
        //list is test class or namespace
        //recount numbers for all children
        for (var i = 0; i < list.children.length; i++) recountTests(list.children[i]);
        
        //if list has no actual values counted from parsing, count the values bz summariying its children
        if ((list.sortParams["testsFailed"] + list.sortParams["testsPassed" + list.sortParams["testsIgnored"]]) == 0) {
            
            for (var i = 0; i < list.children.length; i++) {
                
                list.sortParams["testsFailed"] = list.sortParams["testsFailed"] + list.children[i].sortParams["testsFailed"];
                list.sortParams["testsPassed"] = list.sortParams["testsPassed"] + list.children[i].sortParams["testsPassed"];
                list.sortParams["testsIgnored"] = list.sortParams["testsIgnored"] + list.children[i].sortParams["testsIgnored"];
                
            }
            
        }
        
    }
    
    //values counted for sorting are actual values too
    list.testsPassed = list.sortParams["testsPassed"];
    list.testsFailed = list.sortParams["testsFailed"];
    list.testsIgnored = list.sortParams["testsIgnored"];
    
}

//class represents one node in forest of tests. It corresponds to the HTML report,
//eah instance represents one testStepRunContainer
function TestPart(hash, children, node, show) {
    
    //hash of container counted from HTML
    this.hash = hash;
    //list of children containers in HTML
    this.children = children;
    //DOM node representing TestPart
    this.node = node;
    //boolean variable defining, if the TestPart is to show in report
    this.show = show;
    //array of params to order by
    this.sortParams = new Array();
    //parse name of TestPart from DOM
    this.sortParams["name"] = getTextContent(node.getElementsByClassName("crossref")[0]);
    //parse test results from DOM
    var outcome = node.getElementsByClassName("outcome-icons");
    if (outcome.length > 0){
        this.sortParams["testsPassed"] = parseInt(outcome[0].childNodes[1].data);
        this.sortParams["testsFailed"] = parseInt(outcome[0].childNodes[3].data);
        this.sortParams["testsIgnored"] = parseInt(outcome[0].childNodes[5].data);
    }
    else {
        //no results can be parsed
        this.sortParams["testsPassed"] = 0;
        this.sortParams["testsFailed"] = 0;
        this.sortParams["testsIgnored"] = 0;
    }
    
    //stores, how much child tests with show=true passed, failed and were ignored
    //params for sort are actual values too
    this.testsPassed = this.sortParams["testsPassed"];
    this.testsFailed = this.sortParams["testsFailed"];
    this.testsIgnored = this.sortParams["testsIgnored"];
    
    //parses duration and number of assertions from DOM content
    var detailPanelParts = document.getElementById("detailPanel-"+this.hash).childNodes[0];
    if (detailPanelParts.nodeName == "#text") {
        
        this.sortParams["duration"] = parseFloat(detailPanelParts.data.split(" ")[1].replace(",", "."));
        this.sortParams["assertions"] = parseInt(detailPanelParts.data.split(" ")[4]);
        
    } else {
        //no values were parsed
        this.sortParams["duration"] = 0.0;
        this.sortParams["assertions"] = 0;
        
    }

}

//parse tests from DOM node
function parseTests (item) {
    
    var lis = new Array();
    //iterate throu all the children
    for (var i = 0; i < item.childNodes.length; i++) {
        
        //count with LI with id
        if ((item.childNodes[i].nodeName == "LI") && (item.childNodes[i].id != "")) {
            //parse id of test
            var help1 = item.childNodes[i].id.split("-");
            //construct subtree of test
            var subTree = parseTests (item.childNodes[i]);
            //creats new TestPart
            var help = new TestPart(help1[1], subTree, item.childNodes[i], true);
            lis[lis.length] = help;
            
        }else {
            //parse all the childen, pass result to the parent
            var h = parseTests(item.childNodes[i]);
            if (h.length > 0) lis = h;
        } 
    }
    //returns list of TestParts
    return lis;
}

//remove test details from report
// list is TestPart
function removeTests(list) {
    
    //no children or not shown  
    if (list.children.length == 0) return;
    if (!list.show) return;
    
    //finds corresponding UL in DOM
    var ulEl = document.getElementById("testStepRun-" + list.hash).parentNode;
    
    //remove all the children tests
    for (var i = 0; i < list.children.length; i++) {
        
        removeTests(list.children[i]);    
        
    }
    
    //remove TestPart LI node
    ulEl.removeChild(list.node);
    
}

//draw all tests corresponding to actual data stored in forrest
function drawTests() {
   
   //find first container, draw main list
   var ulEl = document.getElementsByClassName("testStepRunContainer")[0];
   drawList(ulEl, lists[0]);
   
}

//draw (creates) UL node
//parentNode is parent TestPart
//list is TestPart to be drawn
function drawList (parentNode, list) {
    
    //append actual node
    parentNode.appendChild(list.node);
    
    //draw all child TestParts
    if (list.children.length > 0)
        for (var i = 0; i < list.children.length; i++)
            drawList (list.node.getElementsByClassName("testStepRunContainer")[0], list.children[i]);
    
    //remove contents, if TestPart is not to be shown
    if (!list.show) {
        var ulEl = document.getElementById("testStepRun-" + list.hash).parentNode;
        ulEl.removeChild(list.node);
    }
    
}

//orders all tests
function orderTests() {
    
    //select order type from form
    var orderSelect = document.getElementById("order-by");
    orderType = orderSelect.options[orderSelect.selectedIndex].value;

    //removes all tests from report
    removeTests(lists[0]);

    for (var i = 0; i < lists.length; i++) {
        
        //sort all trees of tests
        sortList(lists[0]);
        
    }

    //draw result to report
    drawTests();
    
}

//sort content of TestPart
function sortList(list){
    
    //no content
    if (list.children.length == 0) return;
    
    list.children.sort(compareTests);
    
    //if order is set to desc, reverse result
    var order = document.getElementById ("order");
    var orderClass = order.className;
    if (orderClass == "desc") list.children.reverse();
    
    for (var i = 0; i < list.children.length; i++) {
        
        //sort all the cildren TestParts
        sortList(list.children[i]);
        
    }
    
}
//compate two TestParts based on orderType
function compareTests(a, b) {

    if (a.sortParams[orderType] > b.sortParams[orderType]) return 1;
    else return -1;
}

//action called when click on order arrow is performed
function changeOrder () {
    
    var order = document.getElementById ("order");
    var orderClass = order.className;
    if (orderClass == "asc") {
        order.className = "desc";
        order.src = "files/ArrowUp.png";
    } else {
        order.className = "asc";
        order.src = "files/ArrowDown.png";
    }
    //order changed, sort tests
    orderTests();
    
}

//test to show changed, called when filter or search text changed
function stateChanged(source) {
    
    
    var text = document.getElementsByClassName("searchText")[0].value;
    
    //remove all tests
    removeTests(lists[0]);
    
    //clear search results in forest
    clearSearch(lists[0]);
    
    if (text != "")
        searchList(lists[0], text, false);
    
    //recount numbres of passed, failed and ignored tests
    recountResults(lists[0]);
    
    //apply filte change
    filterChanged();
    
    //draw tests
    drawTests();
    
}

//search TestPart for text, if not found in parent
function searchList(list, text, found) {
    
    //is text found?
    if (list.sortParams['name'].toLowerCase().indexOf(text.toLowerCase()) != -1)
        found = true;
    
    //no children to search in
    if (list.children.length == 0) {
        list.show = found;
        return found;
    }
    
    //search all the children, if found in one, set found to true
    var subfound = false;
    for (var i = 0; i < list.children.length; i++) {
        
        if (searchList(list.children[i], text, found)) subfound = true;
    
    }
    
    found = subfound;
    list.show = found;
    
    return list.show;
    
}

//actual values are the ones for search, reset
function clearSearch (list) {
    
    list.show = true;
    list.testsFailed = list.sortParams["testsFailed"];
    list.testsPassed = list.sortParams["testsPassed"];
    list.testsIgnored = list.sortParams["testsIgnored"];
    if (list.children.length > 0) {
        for (var i = 0; i < list.children.length; i++) {
            clearSearch (list.children[i]);
        }
    }
    
}

//load filters from form
function filterChanged() {
    
    showPassed = document.getElementById("showPassed").checked;
    showFailed = document.getElementById("showFailed").checked;
    showIgnored = document.getElementById("showIgnored").checked;
    
    //apply filters
    applyFilters(lists[0]);
    
}

//set show of TestParts based on filter values
function applyFilters(list) {
    
    if (list.show) {
        
        if (((showPassed) && list.testsPassed > 0) || ((showFailed) && list.testsFailed > 0)
        || ((showIgnored) && list.testsIgnored > 0)) {
        
            for (var i = 0; i < list.children.length; i++) {
                //if TestPart is shown, recount children
                applyFilters(list.children[i]);
            }
            
        } else list.show = false;
        
    }
    
}

//recount test results for all TestPart
function recountResults (list) {
    
    if (list.children.length == 0) {
        
        //test is hidden, no results
        if (!list.show) {
            list.testsPassed = 0;
            list.testsFailed = 0;
            list.testsIgnored = 0;
        }
        return;
    }
    
    if (list.show) {

        //recount all teh children
        for (var i = 0; i < list.children.length; i++) {

            recountResults(list.children[i]);
        }

        list.testsPassed = 0;
        list.testsFailed = 0;
        list.testsIgnored = 0;
        
        //summarize numbers based on children
        for (var i = 0; i < list.children.length; i++) {
            
            if (list.children[i].show) {
            
                list.testsPassed += list.children[i].testsPassed;
                list.testsFailed += list.children[i].testsFailed;
                list.testsIgnored += list.children[i].testsIgnored;
            
            }
        }
    }
}

//action call after click is performed on Reset button of form
function resetSearch () {
    
    document.getElementsByClassName("searchText")[0].value = DEFAULT_SEARCH_TEXT;
    
    stateChanged("reset");
}

//remove default text from search input
function removeDefaultText() {
    
    var searchInput = document.getElementsByClassName("searchText")[0];
    if (searchInput.value == DEFAULT_SEARCH_TEXT) searchInput.value = "";
    
}

function addAccessPanel () {
    
    var h2s = document.getElementsByTagName("h2");
    var  i;
    for (i =0; i < h2s.length; i++) {
        if (h2s[i].innerHTML == "Details") break;
    }
    var h2d = h2s[i];
    h2d.innerHTML = h2d.innerHTML + "<span class=\"accessPanel\">" +
                                        "<form onSubmit=\"stateChanged(); return false;\" onReset=\"resetSearch(); return false;\" id=\"search\"><input onClick=\"removeDefaultText()\" name=\"searchText\" class=\"searchText\" /><input type=\"submit\" value=\"Search\" /><input type=\"reset\" value=\"Clear\" /></form>" +
                                        "<span class=\"dividor\">|</span>" +
                                        "Order by<select id=\"order-by\" onChange=\"orderTests()\">" +
                                            "<option value=\"name\">Name</option>" +
                                            "<option value=\"testsFailed\">Number of tests failed</option>" +
                                            "<option value=\"testsPassed\">Number of tests passed</option>" +
                                            "<option value=\"testsIgnored\">Number of tests ignored</option>" +
                                            "<option value=\"duration\">Duration</option>" +
                                            "<option value=\"assertions\">Number of assertions</option>" +
                                        "</select>" +
                                        "<img id=\"order\" class=\"asc\" src=\"files/ArrowDown.png\" onclick=\"changeOrder()\" />" +
                                        "<span class=\"dividor\">|</span><img src=\"files/PassedShow.gif\" alt=\"Passed\" /><input id=\"showPassed\" type=\"checkbox\" name=\"showPassed\" checked onchange=\"stateChanged()\" />" +
                                        "<span class=\"dividor\">|</span><img src=\"files/FailedShow.gif\" alt=\"Show Failed\" /><input id=\"showFailed\" type=\"checkbox\" name=\"showIgnored\" checked onchange=\"stateChanged()\"/>" +
                                        "<span class=\"dividor\">|</span><img src=\"files/IgnoredShow.gif\" alt=\"Show Ignored\" /><input id=\"showIgnored\" type=\"checkbox\" name=\"showIgnored\" checked onchange=\"stateChanged()\" />" +
                                     "</span>";
    
}